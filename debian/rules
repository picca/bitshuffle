#!/usr/bin/make -f
# See debhelper(7) (uncomment to enable)
# output every command that modifies files on the build system.
export DH_VERBOSE = 1

export PYBUILD_NAME=bitshuffle

# see FEATURE AREAS in dpkg-buildflags(1)
export DEB_BUILD_MAINT_OPTIONS = hardening=+all

# see ENVIRONMENT in dpkg-buildflags(1)
# package maintainers to append CFLAGS
#export DEB_CFLAGS_MAINT_APPEND  = -Wall -pedantic
# package maintainers to append LDFLAGS
#export DEB_LDFLAGS_MAINT_APPEND = -Wl,--as-needed

export ENABLE_ZSTD

ifneq ($(wildcard /usr/lib/$(DEB_HOST_MULTIARCH)/hdf5/openmpi/libhdf5.so),)
  HDF5_DIR := /usr/lib/$(DEB_HOST_MULTIARCH)/hdf5/openmpi
else
  HDF5_DIR := /usr
endif

DESTDIR=$(CURDIR)/debian/tmp
FLAVOURS=serial openmpi
LZF_SRC=src/lzf_h5plugin.c lzf/lzf_filter.c
BITSHUFFLE_SRC=src/bshuf_h5plugin.c src/bshuf_h5filter.c src/bitshuffle.c src/bitshuffle_core.c src/iochain.c

%:
	dh $@ --buildsystem=pybuild

override_dh_auto_build:
	CC=mpicc dh_auto_build

	# build the hdf5 plugins for hdf5 flavours
	for flavour in $(FLAVOURS); do\
		builddir=$(CURDIR)/build-$$flavour;\
		mkdir -p $$builddir;\
		$(CC) $(CFLAGS) $(CPPFLAGS) $(LDFLAGS) -shared -std=c99 -ffast-math -pthread -O3 -fPIC \
			-DBSHUF_VERSION_MAJOR=0 -DBSHUF_VERSION_MINOR=5 -DBSHUF_VERSION_POINT=1 \
			-DZSTD_SUPPORT=1 \
			-I$(CURDIR)/lzf $(LZF_SRC) \
			`pkg-config --cflags --libs hdf5-$$flavour` \
			`pkg-config --cflags --libs liblzf` \
			`pkg-config --cflags --libs zstd` \
			-o $$builddir/libh5LZF.so;\
		$(CC) $(CFLAGS) $(CPPFLAGS) $(LDFLAGS) -shared -std=c99 -ffast-math -pthread -O3 -fPIC \
			-DBSHUF_VERSION_MAJOR=0 -DBSHUF_VERSION_MINOR=5 -DBSHUF_VERSION_POINT=1 \
			-DZSTD_SUPPORT=1 \
			-I$(CURDIR)/src $(BITSHUFFLE_SRC) \
			`pkg-config --cflags --libs hdf5-$$flavour` \
			`pkg-config --cflags --libs liblz4` \
			`pkg-config --cflags --libs zstd` \
			-o $$builddir/libh5bshuf.so;\
	done


override_dh_auto_test:
	mpirun --allow-run-as-root -n 1 dh_auto_test

override_dh_auto_install:
	for flavour in $(FLAVOURS); do\
		pdir=$(DESTDIR)/`pkg-config --variable=PluginDir hdf5-$$flavour`;\
		builddir=$(CURDIR)/build-$$flavour;\
		install -d $$pdir;\
		install -t $$pdir $$builddir/*;\
	done

	dh_auto_install

override_dh_install:
	dh_install
	dh_install -p bitshuffle /usr

override_dh_auto_clean:
	rm -rf lzf/lzf
	rm -rf lz4

	dh_auto_clean
